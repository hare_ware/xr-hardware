#!/usr/bin/env python3 -i
# Copyright 2019-2020 Collabora, Ltd
# SPDX-License-Identifier: BSL-1.0
# Author: Ryan Pavlik <ryan.pavlik@collabora.com>
"""XR Hardware device database."""

from .device import Device

# Please make sure VID/PID are lowercase.


def get_devices():
    """Return the entire device database in one really large tuple."""
    return (
        Device("Razer Hydra", "1532", "0300"),
        Device("HTC Vive", "0bb4", "2c87"),
        Device("HTC Vive Pro", "0bb4", "0309"),
        Device("Valve Watchman Dongle", "28de", "2101"),
        Device("Valve VR Radio", "28de", "2102"),
        Device("Valve Index Controller", "28de", "2300"),
        # Name might be wrong.
        Device("Valve Receiver for Lighthouse - HTC Vive", "28de", "2000"),
        # also PID 2220 (lighthouse fpga rx), 2300 (lhr vive pro?)
        Device("OSVR HDK", "1532", "0b00", usb_serial_name="OSVRHDK"),
        Device("OSVR HDK Camera", "0bda", "57e8"),
        Device("Sensics zSight", "16d0", "0515", usb_serial_name="zSight"),

        # Re-used ID from ST Micro "LED badge -- mini LED display -- 11x44"
        Device("NOLO CV1", "0483", "5750"),
        # Unknown where this VID was assigned
        Device("NOLO CV1", "28e9", "028a"),

        # ST Micro VID, and PID not listed in usb-ids so may be uniquely
        # assigned (or may be just copied from PID of DK2 since the product
        # string is "Rift P1")
        Device("Pimax 4k", "0483", "0021"),

        Device("Oculus Rift (DK1)", "2833", "0001"),
        Device("Oculus Rift (DK2)", "2833", "0021"),
        Device("Oculus Rift (DK2)", "2833", "2021"),
        Device("Oculus Rift (DK2 Sensor)", "2833", "0201"),
        Device("Oculus Rift (CV1)", "2833", "0031"),
        Device("Oculus Rift (CV1 Sensor)", "2833", "0211"),
        Device("Oculus Rift S", "2833", "0051"),
        Device("Samsung GearVR (Gen1)", "04e8", "a500"),

        Device("3Glasses-D3V1", "2b1c", "0200"),
        Device("3Glasses-D3V2", "2b1c", "0201"),
        Device("3Glasses-D3C", "2b1c", "0202"),
        Device("3Glasses-D2C", "2b1c", "0203"),
        Device("3Glasses-S1V5", "2b1c", "0100"),
        Device("3Glasses-S1V8", "2b1c", "0101"),

        # Need to be ignored by libinput to avoid spurious touchscreen events
        Device("Microsoft Windows MR Controller", "045e", "065b",
               bluetooth=True, usb=False,
               extra_properties={"LIBINPUT_IGNORE_DEVICE": "1"}),
        Device("Microsoft Windows MR Controller", "045e", "065d",
               bluetooth=True, usb=False,
               extra_properties={"LIBINPUT_IGNORE_DEVICE": "1"}),
        Device("Microsoft Windows MR Controller (Reverb G2)", "045e", "066a",
               bluetooth=True, usb=False,
               extra_properties={"LIBINPUT_IGNORE_DEVICE": "1"}),

        Device("Sony PlayStation VR", "054c", "09af"),

        Device("Sony PlayStation Move Motion Controller CECH-ZCM1",
               "054c", "03d5", bluetooth=True, usb=True),
        Device("Sony PlayStation Move Motion Controller CECH-ZCM2",
               "054c", "0c5e", bluetooth=True, usb=True),
        # TODO Duplicate of NOLO CV1?
        # Device("Deepoon", "0483", "5750"),

        Device("LG 360 VR R-100", "1004", "6374"),

        Device("Microsoft HoloLens Sensors", "045e", "0659"),
        Device("Samsung Odyssey+ sensors", "04e8", "7312"),
        Device("HP Reverb G1", "03f0", "0c6a"),
        Device("HP Reverb G2", "03f0", "0580"),
        Device("Lenovo QHMD", "17ef", "b801"),
        Device("Cypress Semiconductor Corp. Lenovo Explorer", "04b4", "6504"),

        # Pretends to be a DK1...
        # Device("VR-Tek WVR1", "2833", "0001"),

        # Vis3r NxtVR
        Device("Vis3r NxtVR", "1209", "9d0f"),
    )
