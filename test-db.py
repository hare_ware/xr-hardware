#!/usr/bin/env python3
# Copyright 2019-2020 Collabora, Ltd
# SPDX-License-Identifier: BSL-1.0
# Author: Ryan Pavlik <ryan.pavlik@collabora.com>
"""XR Hardware device database consistency test."""
from xrhardware.db import get_devices

if __name__ == "__main__":
    print("Testing device database entries' consistency and uniqueness")
    known = set()
    import re
    hexstring = re.compile(r"[0-9a-fA-F]{4}")
    for dev in get_devices():

        print("Checking entry for:", dev.extended_description)

        # Duplicate ID?
        for dev_id in dev.yield_hwdb_identification():
            assert(dev_id not in known)
            known.add(dev_id)

        # Must be at least one of these
        assert(dev.usb or dev.bluetooth)

        # VID/PID must fit the expected format
        if dev.pid:
            assert(hexstring.match(dev.pid))
            # prefer lowercase for compatibility
            assert(dev.pid == dev.pid.lower())
        if dev.vid:
            assert(hexstring.match(dev.vid))
            # prefer lowercase for compatibility
            assert(dev.vid == dev.vid.lower())
    print("Success!")
