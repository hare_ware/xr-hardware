# Changelog for xr-hardware

<!--
SPDX-License-Identifier: CC0-1.0
SPDX-FileCopyrightText: 2020-2021 Collabora, Ltd. and the xr-hardware contributors
-->

## xr-hardware 1.0.0 (2021-10-25)

Version bumped because there was no real reason to be pre-1.0 anymore,
this package is widely used now.

- Additions
  - Add USB PID for Oculus Rift CV1 sensors, needed for positional tracking
    support.
    ([!14](https://gitlab.freedesktop.org/monado/utilities/xr-hardware/merge_requests/14))
  - Add USB PID for Oculus Rift DK2 sensor, needed for positional tracking support.
    ([!15](https://gitlab.freedesktop.org/monado/utilities/xr-hardware/merge_requests/15))
- Improvements
  - Migrate script from attrs to dataclasses (which is built-in to Python 3.7 and
    newer)
    ([!16](https://gitlab.freedesktop.org/monado/utilities/xr-hardware/merge_requests/16))
  - Generate copyright header into generated files, and other minor changes to meet
    REUSE 3.0 compliance: see <https://reuse.software>.
    ([!17](https://gitlab.freedesktop.org/monado/utilities/xr-hardware/merge_requests/17))

## xr-hardware 0.4.0 (2021-02-10)

- Additions
  - HP Reverb G1 & G2
    ([!12](https://gitlab.freedesktop.org/monado/utilities/xr-hardware/merge_requests/12))
- Improvements
  - Add validators to `vid`, `pid`, and `usb_serial_name` fields directly in the
    class, for better error detection.
    ([!13](https://gitlab.freedesktop.org/monado/utilities/xr-hardware/merge_requests/13),
    [#4](https://gitlab.freedesktop.org/monado/utilities/xr-hardware/issues/4))

## xr-hardware 0.3.0 (2020-12-02)

Note that the default branch has been renamed to "main" in the repository.

- Additions
  - Oculus Rift S
    ([!5](https://gitlab.freedesktop.org/monado/utilities/xr-hardware/merge_requests/5),
    [#3](https://gitlab.freedesktop.org/monado/utilities/xr-hardware/issues/3))
  - Windows MR controller support
    ([!6](https://gitlab.freedesktop.org/monado/utilities/xr-hardware/merge_requests/6),
    [#1](https://gitlab.freedesktop.org/monado/utilities/xr-hardware/issues/1))
  - Vis3r NxtVR budget VR headset
    ([!7](https://gitlab.freedesktop.org/monado/utilities/xr-hardware/merge_requests/7))
  - Pimax 4k headset
    ([!10](https://gitlab.freedesktop.org/monado/utilities/xr-hardware/merge_requests/10),
    [#5](https://gitlab.freedesktop.org/monado/utilities/xr-hardware/issues/5))
- Improvements
  - Valve Index: ID for Watchman device
    ([!8](https://gitlab.freedesktop.org/monado/utilities/xr-hardware/merge_requests/8))
  - Now using [Proclamation](https://gitlab.com/ryanpavlik/proclamation) to
    maintain a project changelog.
    ([!9](https://gitlab.freedesktop.org/monado/utilities/xr-hardware/merge_requests/9))
  - NOLO CV1: Additional ID
    ([!10](https://gitlab.freedesktop.org/monado/utilities/xr-hardware/merge_requests/10),
    [#5](https://gitlab.freedesktop.org/monado/utilities/xr-hardware/issues/5))
  - Additional IDs for HTC Vive Pro
    ([!11](https://gitlab.freedesktop.org/monado/utilities/xr-hardware/merge_requests/11),
    [#6](https://gitlab.freedesktop.org/monado/utilities/xr-hardware/issues/6))

## xr-hardware 0.2.1 (2020-01-22)

- Fixes
  - Vive support

## xr-hardware 0.2 (2020-01-22)

- Additions
  - Support for Bluetooth devices
  - 3Glasses devices
  - PS Move controllers
- Improvements
  - Refactor generated files
  - Re-organize code and improve testing/quality

## xr-hardware 0.1.1 (2020-01-21)

- Fixes
  - Fix a typo related to HTC Vive support.

## xr-hardware 0.1 (2020-01-21)

Initial release.
